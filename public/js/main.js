$(document).ready(function(){
    $(function() {
      $('a#scroll-down').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop:$('#header').height()-77}, 'slow', 'linear');
      });
    });
});