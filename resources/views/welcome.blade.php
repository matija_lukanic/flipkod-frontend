<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Flipkod frontend</title>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css/custom.css"> 
        <link rel="stylesheet" href="css/masonry.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


    </head>
    <body>

        <nav class="navbar navbar-expand-lg pt-3 pb-3 pl-5 pr-5 fixed-top navbar-light">
          <a class="navbar-brand pl-0 pl-md-5 text-white" href="#">
            <img src="img/logo.png" class="img-responsive" />
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse justify-content-end pr-5" id="navbarNavAltMarkup">
            <div class="navbar-nav text-uppercase align-items-center">
              <a class="nav-item nav-link pr-3 pl-3 text-white" href="#">Home</a>
              <a class="nav-item nav-link pr-3 pl-3 text-white" href="#">Features</a>
              <a class="nav-item nav-link pr-3 pl-3 text-white" href="#">About us</a>
              <a class="nav-item nav-link pr-3 pl-3 text-white" href="#">Portfolio</a>
              <a class="nav-item nav-link pr-3 pl-3 text-white" href="#">Services</a>
              <a class="nav-item nav-link pr-3 pl-3 text-white" href="#">Contact us</a>
              <a class="nav-item nav-link pl-0 pl-md-5 text-white" href="#"><i class="fa fa-search"></i></a>
            </div>
          </div>

        </nav>

        <div class="d-flex align-items-center justify-content-center h-100 text-white" id="header">

            <div class="col-4"></div>
            <div class="col-12 col-md-4 p-2 text-center">
                <p class="text-uppercase ls txt-sm">Feeder is here</p>
                <h1 class="font-weight-light mb-4">Free Sketch3 One Page Theme Ready For Coding</h1>
                <p>Well, the way they make shows is, they make one show</p>
            </div>
            <div class="col-4"></div>

            <a href="#" class="hidden-xs hidden-sm" id="scroll-down">
                <span></span>
            </a>

        </div>


        <div class="d-flex align-items-center justify-content-center bg-white p-5">

            <div class="col-2"></div>
            <div class="col-12 col-md-8 p-0 text-center">
                <h2 class="font-weight-normal mb-4 pb-2 mt-4">About Feeder</h2>
                <p class="mb-5 p-intro text-black-50">                    
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eu aliquam ipsum. Nulla vehicula erat porta, rhoncus dolor quis, facilisis ipsum. Aenean ullamcorper, odio in tempus suscipit, mi sem feugiat lectus, ac pharetra libero quam non metus. Etiam hendrerit sit amet metus eu feugiat. Vestibulum lobortis, sem eget varius vulputate, dui magna varius mi, vel ultricies arcu ligula ac ligula. Cras non quam id sapien vehicula commodo. Ut ultricies justo et commodo aliquam. Integer congue aliquam sem. Nulla vulputate enim ac pulvinar pharetra. Nullam non dui ornare, pulvinar mauris et, luctus quam. Nulla eu vulputate mi, at suscipit nunc.
                </p>

            </div>
            <div class="col-2"></div>
        </div>



        <div class="d-flex align-items-center flex-md-row flex-column justify-content-center why-div">

            <div class="col-12 col-md-6 left-div align-self-stretch">
                
            </div>
            <div class="col-12 col-md-6 right-div p-5">
                <div class="col-12 col-md-10 p-3 text-center text-md-left">
                    <h2 class="font-weight-normal mb-5 mt-5">Why choose Feeder</h2>
                    <p class="mb-4 p-intro text-black-50">                    
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eu aliquam ipsum. Nulla vehicula erat porta, rhoncus dolor quis, facilisis ipsum. Aenean ullamcorper, odio in tempus suscipit, mi sem feugiat lectus, ac pharetra libero quam non metus. Etiam hendrerit sit amet metus eu feugiat. Vestibulum lobortis, sem eget varius vulputate, dui magna varius mi, vel ultricies arcu ligula ac ligula. Cras non quam id sapien vehicula commodo. Ut ultricies justo et commodo aliquam. Integer congue aliquam sem. Nulla vulputate enim ac pulvinar pharetra. Nullam non dui ornare, pulvinar mauris et, luctus quam. Nulla eu vulputate mi, at suscipit nunc.
                    </p>
                </div>
            </div>
        </div>

        <div class="d-flex align-items-center justify-content-center bg-white pt-5 pb-2">

            <div class="col-2"></div>
            <div class="col-12 col-md-8 p-0 text-center">
                <h2 class="font-weight-normal mb-4 pb-2 mt-4">Portfolio</h2>
                <p class="p-intro text-black-50 mb-4">                    
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </p>
            </div>
            <div class="col-2"></div>

        </div>

        
        <div class="button-group filters-button-group mb-5 pb-3 pt-3">
            <button id="all-masonry" class="mb-3 ls text-uppercase font-weight-bolder button is-checkedpl-0 pr-0 ml-4 mr-4" data-filter="*">All</button>
            <button class="mb-3 ls text-uppercase font-weight-bolder button pl-0 pr-0 ml-4 mr-4" data-filter=".design">Design</button>
            <button class="mb-3 ls text-uppercase font-weight-bolder button pl-0 pr-0 ml-4 mr-4" data-filter=".identity">Identity</button>
            <button class="mb-3 ls text-uppercase font-weight-bolder button pl-0 pr-0 ml-4 mr-4" data-filter=".photography">Photography</button>
            <button class="mb-3 ls text-uppercase font-weight-bolder button pl-0 pr-0 ml-4 mr-4" data-filter=".web">Web design</button>
        </div>

        <section id="grid-container" class="transitions-enabled fluid masonry js-masonry grid">
            <article class="design">
                <img src="img/design1.jpg" class="img-responsive" />
            </article>
            <article class="design">
                <img src="img/design2.jpg" class="img-responsive" />
            </article>
            <article class="design">
                <img src="img/design3.jpg" class="img-responsive" />
            </article>
            <article class="identity">
                <img src="img/identity.png" class="img-responsive" />
            </article>
            <article class="photography">
                <img src="img/photo1.jpg" class="img-responsive" />
            </article>
            <article class="photography">
                <img src="img/photo2.jpg" class="img-responsive" />
            </article>
            <article class="photography">
                <img src="img/photo3.jpg" class="img-responsive" />
            </article>
            <article class="web">
                <img src="img/web1.jpg" class="img-responsive" />
            </article>
            <article class="web">
                <img src="img/web2.jpg" class="img-responsive" />
            </article>
        </section>


    <script src="js/main.js" type="text/javascript" ></script>
    <script src="js/masonry.js" type="text/javascript" ></script>
    </body>
</html>
